"""
ex39
"""

import hashmap

def hr_line():
    """ line """
    print '-' * 10

def main():
    """ main """
    states = hashmap.new()
    hashmap.set(states, 'Oregon', 'OR')
    hashmap.set(states, 'Florida', 'FL')
    hashmap.set(states, 'California', 'CA')
    hashmap.set(states, 'New York', 'NY')
    hashmap.set(states, 'Michigan', 'MI')

    cities = hashmap.new()
    hashmap.set(cities, 'CA', 'San Francisco')
    hashmap.set(cities, 'MI', 'Detroit')
    hashmap.set(cities, 'FL', 'Jacksonville')

    hashmap.set(cities, 'NY', 'New York')
    hashmap.set(cities, 'OR', 'Portland')

    hr_line()
    print "NY State has: %s" % hashmap.get(cities, 'NY')
    print "OR State has: %s" % hashmap.get(cities, 'OR')

    hr_line()
    print "Michigan's abbreviation is: %s" % hashmap.get(states, 'Michigan')
    print "Florida's abbreviation is: %s" % hashmap.get(states, 'Florida')

    hr_line()
    print "Michigan has: %s" % hashmap.get(
        cities, hashmap.get(states, 'Michigan'))
    print "Florida has: %s" % hashmap.get(
        cities, hashmap.get(states, 'Florida'))

    hr_line()
    hashmap.list(states)

    hr_line()
    hashmap.list(cities)

    hr_line()
    state = hashmap.get(states, 'Texas')

    if not state:
        print "Sorry, no Texas."

    city = hashmap.get(cities, 'TX', 'Does Not Exist')
    print "The city for the state 'TX' is: %s" % city

main()
