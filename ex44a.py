class Parent(object):
    def implicit(self):
        print "PARENT implicit()"
    def override(self):
        print "PARENT override()"
    def altered(self):
        print "PARENT altered"
class Child(Parent):
    pass 
    def override(self):
        print "CHILD override()"
    def altered(self):
        print "CHILD before altered"
        super(Child, self).altered()
        print "CHILD after altered"

dad = Parent()
son = Child()

dad.implicit()
son.implicit()

dad.override()
son.override()

dad.altered()
son.altered()
