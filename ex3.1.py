# printing
print "I will now count my chickens:"
# print math
print "Hens", 25+30/6
# print math, остаток
print "Roosters", 100 - 25 * 3 % 4
# print
print "Now I will count the eggs:"
# math
print 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6
#print
print "Is it true that 3+ 2 < 5  - 7?"
# bool
print 3 + 2 < 5 - 7
#print
print "What is 3 + 2?", 3+2
#print
print "What is 5 - 7?", 5 - 7
#print
print "Oh, that's why it's False."
#print
print "How about some more."

print "Is it greater?", 5 > -2
print "Is it greater or equal?", 5 >= -2
print "Is it less or equal?", 5 <= -2
