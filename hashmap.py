""" hashmap module """
def new(num_buckets=256):
    """Initializes a Map with the given number of buckets."""
    a_map = []
    for i in range(0, num_buckets):
        a_map.append([])
    return a_map
def hash_key(a_map, key):
    """Given a key this will create a number and then convert it to
    an index for the aMap's buckets."""
    return hash(key) % len(a_map)
def get_bucket(a_map, key):
    """
    Given a key, find the bucket where it would go.
    """
    bucket_id = hash_key(a_map, key)
    return a_map[bucket_id]

def get_slot(a_map, key, default = None):
    """
    Returns the index, key, and value of a slot found in a bucket.
    Returns -1, key, and defaulr (None if not set when not found.
    """
    bucket = get_bucket(a_map, key)
    for i, keyvalue in enumerate(bucket):
        k, val = keyvalue
        if key == k:
            return i, k, val
    return -1, key, default

def get(a_map, key, default=None):
    """ Gets the value in a bucket for the given key, or the default. """
    i, k, v = get_slot(a_map, key, default=default)
    return v

def set(a_map, key, value):
    """ Sets the key to the value, replacing any existing value. """
    bucket = get_bucket(a_map, key)
    i, k, v = get_slot(a_map, key)
    if i >= 0:
        bucket[i] = (key, value)
    else:
        bucket.append((key,value))
def delete(a_map, key):
    """ Deletes the given key from the Map. """
    bucket = get_bucket(a_map, key)
    for i in xrange(len(bucket)):
        _key, _val = bucket[i]
        if key == _key:
            del bucket[i]
            break
def list(a_map):
    """prints out what's in the map."""
    for bucket in a_map:
        if bucket:
            for k, v in bucket:
                print k, v
